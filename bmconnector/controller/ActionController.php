#!/usr/bin/php
<?php

/**
 * Controller de entrada do sistema
 */
class ActionController {
	
/**
 * executa o metodo predictive no aplicativo remoto
 */
	public function predictive($uniqueid='',$origin='',$destination='',$linkedid) {
		$confs = new Bootstrap();
		$url = sprintf("http://%s/%s/connector/predictive/%s/%s/%s/%s",
				$confs->read('System.host'),
				$confs->read('System.name'),
				$uniqueid,
				$this->clean($origin),
				$this->clean($destination),
				$this->clean($linkedid)
		);
		return $url;
	}	
	
/**
 * executa o metodo answer_sainte no aplicativo remoto
 */
	public function answer_sainte($uniqueid='',$linkedid='') {
		$confs = new Bootstrap();
		$url = sprintf("http://%s/%s/connector/answer_sainte/%s/%s",
				$confs->read('System.host'),
				$confs->read('System.name'),
				$uniqueid, 
				$linkedid
		);
		return $url;
	}	
	
/**
 * executa o metodo close_sainte no aplicativo remoto
 */
	public function close_sainte($uniqueid='') {	
		$confs = new Bootstrap();
		$url = sprintf("http://%s/%s/connector/close_sainte/%s",
				$confs->read('System.host'),
				$confs->read('System.name'),
				$uniqueid
		);
		return $url;
	}	
	
/**
 * Executa uma consulta no sistema para saber se o numero
 * ainda esta ativo.
 * Quando um numero sai do discador preditivo e vai para
 * a fila, ele recebeum hangup, esse hangup é falso e deve
 * ser ignorado
 */	
	public function validateHangup($callerid) {
		
		$output = shell_exec(
				'asterisk -rx "core show channels verbose" | grep ' .$callerid. ' | wc -l');
		echo "<pre>$output</pre>";
		
		return true;
	}
	
/**
 * executa o metodo validate_pass no aplicativo remoto
 */
	public function validate_pass($exten='', $uniqueid='') {
		$confs = new Bootstrap();
		$url = sprintf("http://%s/%s/connector/validate_pass/%s/%s",
				$confs->read('System.host'),
				$confs->read('System.name'),
				$this->clean($exten),
				$uniqueid
		);
		return $url;
	}		
		
/**
 * limpa a string passada em value 
 */
	public function clean($value) {
		return str_replace("/", "=D5", $value);
	}
}
?>
