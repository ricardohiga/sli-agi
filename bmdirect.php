#!/usr/bin/php
<?php

/**
 * Imports
 */
require_once ('bmconnector/config/Bootstrap.php');
require_once ('bmconnector/dao/OfflineCallDAO.php');
require_once ('bmconnector/persistence/Persistence.php');
require_once ('phpagi/phpagi.php');

/**
 * Instanciando os objetos de classe
 */
$agi = new AGI();
$offline = OfflineCallDAO();

/**
 * Data utc
 */
$timezone = new DateTimeZone('UTC');
$date = new DateTime("now", $timezone);
$utc = $date->format('Y-m-d H:i:s');

switch (strtolower($argv[1])) {
			
	case 'answersainte':
			// exten
			$offline->answerSainte($argv[2]);
		break;
		
	case 'answerentrante':
			// exten, origin, dstchannel			
			$offline->answerEntrante($argv[2],$argv[3],$argv[4]);
		break;
		
	case 'answerentrantedial':
			// exten, origin, dstchannel			
			$offline->answerEntranteDial($argv[2]);
		break;

	default :
		$agi->noop('Metodo ' .$controller->parameter['app']. ' nao encontrado');
		break;	
		
}

$agi->noop('url: ' .$url);
$agi->noop('finalizando');

exit ();
?>

